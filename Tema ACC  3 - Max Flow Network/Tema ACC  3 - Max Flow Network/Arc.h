#pragma once
#include <string>
class Arc
{
public:
	Arc();
	Arc(const std::string& first, const std::string& second, double lowerCapacity, double totalCapacity, int cost);

public:
	int compareTo(const Arc& other);
	void reverse();
public:
	struct Comparator
	{
		bool operator()(const Arc& arcOne, const Arc& arcTwo) const
		{
			if (arcOne.totalCapacity < arcTwo.totalCapacity) return true;
			return false;
		}
	};
public:
	friend bool operator ==(const Arc& arc1, const Arc& arc2);
public:
	std::string first;
	std::string second;
	int cost;
	double currentCost;
	double totalCapacity;
	double lowerCapacity;
	int type = 1;
};