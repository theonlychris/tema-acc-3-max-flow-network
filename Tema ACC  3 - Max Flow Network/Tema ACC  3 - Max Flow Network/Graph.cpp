#include "Graph.h"

Graph::Graph(const std::vector<Node> & nodes, const std::vector<Arc> & arcs) : nodes(nodes), arcs(arcs)
{
// Empty
}

void Graph::setUp()
{
	for (Arc iterator : arcs)
	{
		nodes[getNodeByName(iterator.first)].arcs.push_back(iterator);
		nodes[getNodeByName(reverse(iterator).first)].arcs.push_back(reverse(iterator));
	}
	for (Node iterator : nodes)
	{
		std::sort(iterator.arcs.begin(), iterator.arcs.end(), Arc::Comparator());
	}
}

int Graph::getNodeByName(const std::string& node)
{
	for (int counter = 0; counter < nodes.size(); counter++)
	{
		if (nodes[counter].name[0] == node[0])
			return counter;
	}
	return 0;
}

void Graph::print()
{
	for (Node iterator : nodes)
	{
		std::cout << iterator.name << "-> ";
		for (Arc arc : iterator.arcs)
		{
			std::cout << "<" << arc.first << "," << arc.second << ",";
			std::cout << "<" << arc.first << "," << arc.second << "," << arc.type << ">";

		}
		std::cout << std::endl;
	}
}

Arc Graph::reverse(const Arc& arc)
{
	Arc newArc(arc.second, arc.first, arc.lowerCapacity, arc.totalCapacity, arc.cost);
	newArc.type *= -1;
	return newArc;
}
