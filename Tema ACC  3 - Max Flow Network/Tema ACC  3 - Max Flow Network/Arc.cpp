#include "Arc.h"

Arc::Arc()
{
	currentCost = 0;
	totalCapacity = 0;
	first = "";
	second = "";
	cost = 0;
}

Arc::Arc(const std::string& first, const std::string& second, double lowerCapacity, double totalCapacity, int cost) : first(first), second(second), totalCapacity(totalCapacity), lowerCapacity(lowerCapacity), currentCost(currentCost), cost(cost)
{
	// Empty
}

void Arc::reverse()
{
	std::string aux = first;
	first = second;
	second = aux;
	this->type *= -1;
}

int Arc::compareTo(const Arc& other)
{
	if (this->totalCapacity > other.totalCapacity)
	{
		return -1;
	}
	else if (this->totalCapacity < other.totalCapacity)
	{
		return 1;
	}
	return 0;
}

bool operator==(const Arc& arc1, const Arc& arc2)
{
	if (arc1.first == arc2.first && arc1.second == arc2.second)
		return true;
	return false;
}
