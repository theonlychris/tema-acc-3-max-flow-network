#pragma once
#include "Node.h"
#include <iostream>
#include <algorithm>
class Graph
{
public:
	std::vector<Node> nodes;
	std::vector<Arc> arcs;
public:
	Graph() = default;
	Graph(const std::vector<Node> & nodes, const std::vector<Arc> & arcs);
public:
	void setUp();
	int getNodeByName(const std::string& node);
	void print();
	Arc reverse(const Arc& arc);
};

