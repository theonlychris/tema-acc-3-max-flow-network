#include "MaxFlow.h"
#include "fstream"

std::vector<Arc> extractSolution(std::vector<std::vector <Arc>> foundPaths)
{
	std::vector<Arc> solution;
	for (std::vector<Arc> list : foundPaths)
	{
		solution.push_back(list[list.size() - 2]);
	}
	return solution;
}

int main()
{
	
	std::ifstream myfile("input.txt");
	std::vector<Node> nodes;
	std::vector<Arc> arcs;
	int nrOfBoys;
	myfile >> nrOfBoys;
	int nrOfCommitees;
	myfile >> nrOfCommitees;
	nodes.push_back(Node("S"));
	nodes.push_back(Node("T"));

	for (int index = 1; index <= nrOfBoys; ++index)
	{
		std::string name = std::to_string(index);
		Node newNodename(name);
		nodes.push_back(newNodename);
		Arc newArc("S", name, 0, 1, 0);
		arcs.push_back(newArc);
	}

	for (int index = 0; index < nrOfCommitees; ++index)
	{
		char c = (char)(index + 'A');
		std::string commitee(1, c);
		Node newNodeComitee(commitee);
		nodes.push_back(newNodeComitee);
		Arc newArc(commitee, "T", 0, 1, 0);
		arcs.push_back(newArc);
	}
	nrOfBoys += nrOfCommitees + 2;
	Node newNodeT("T");
	nodes.push_back(newNodeT);

	while (!myfile.eof())
	{
		std::string currentCommitee;
		myfile >> currentCommitee;
		std::string boy;
		myfile >> boy;
		while (!(boy == "-"))
		{
			Arc newArc(boy, currentCommitee, 0, 1, 0);
			arcs.push_back(newArc);
			myfile >> boy;
		}
	}
	Graph graph(nodes, arcs);
	MaxFlow maxFlow(graph, graph.nodes[graph.getNodeByName("S")], graph.nodes[graph.getNodeByName("T")]);
	std::cout << "Max flow is " << maxFlow.getMaxFlux() << std::endl;
	std::vector<Arc> solution = extractSolution(maxFlow.foundPaths);
	for (Arc arc : solution)
	{
		std::cout << arc.first << " is asigned as president to " << arc.second << " commitee";
	}
	return 0;
	

}