#pragma once
#include "Graph.h"
#include <limits>

class MaxFlow
{
	
public:
	double totalFlow = 0;
	std::vector<Node> nodes;
	std::vector<Arc> arcs;
	std::vector<Arc> path;
	std::vector<std::vector <Arc>> foundPaths;
	Graph graph;
	Node source;
	Node target;
public:
	MaxFlow() = default;
	MaxFlow(const Graph &graph, const Node &source, const Node &target) : graph(graph), nodes(graph.nodes), arcs(graph.arcs), source(source), target(target)
	{
	//Empty
	}
	
	double DFS(Node currentNode, double currentFlow) 
	{
		for (Arc currentArc : currentNode.arcs) 
		{
			if (!checkNodeVisited(path, currentArc)) 
			{
				if (currentArc.type == 1) {
					if (currentArc.currentCost < currentArc.totalCapacity) 
					{
						if (currentFlow > currentArc.totalCapacity - currentArc.currentCost) {
							currentFlow = currentArc.totalCapacity + currentArc.lowerCapacity - currentArc.currentCost;
						}
						path.push_back(currentArc);
						if (currentArc.second[0] == target.name[0]) 
						{
							return currentFlow;
						}
						else 
						{
							double flux = currentFlow;
							currentFlow = DFS(nodes[graph.getNodeByName(currentArc.second)], flux);
							if (currentFlow != 0)
								return currentFlow;
							currentFlow = flux;
							path.erase(path.begin() + (path.size() - 1));
						}
					}
				}
				else if (currentArc.type == -1) 
				{
					if (currentArc.currentCost > currentArc.lowerCapacity) 
					{
						if (currentArc.currentCost < currentFlow) 
						{
							currentFlow = currentArc.currentCost;
						}
						path.push_back(currentArc);
						if (currentArc.second[0] == target.name[0])
						{
							return currentFlow;
						}
						else 
						{
							double flux = currentFlow;
							currentFlow = DFS(nodes[graph.getNodeByName(currentArc.second)], flux);
							if (currentFlow != 0)
								return currentFlow;
							currentFlow = flux;
							path.erase(path.begin() + (path.size() - 1));
						}
					}
				}
			}
		}
		return 0;
	}
	template <typename T>
		bool twoVectorsAreEqual(const std::vector<T> & v1, const std::vector<T> & v2)
		{
			for (int i = 0; i < v1.size(); ++i)
			{
				if (!(v1[i] == v2[i]))
					return false;
			}
			return true;
		}
	bool checkPathVisited(std::vector<Arc> path) 
	{
		for (std::vector<Arc> iterator : foundPaths)
		{
			if (twoVectorsAreEqual(path,iterator))
			{
				return true;
			}
		}
		return false;
	}
	

	
	bool checkNodeVisited(std::vector<Arc> path, Arc arc) 
	{
		for (Arc iterator : path) 
		{
			if ((iterator.first[0] == arc.first[0]) && (iterator.second[0] == arc.second[0])
				|| ((iterator.first[0] == arc.second[0]) && (iterator.second[0] == arc.first[0]))) 
			{
				return true;
			}
		}
		return false;
	}
	

	double getMaxFlux() 
	{
		double flow = DFS(source, std::numeric_limits<int>::max());
		while (flow != 0) 
		{
			foundPaths.push_back(path); // wtf is this
			update(path, flow);
			path.clear();
			totalFlow += flow;
			flow = DFS(source, 100);
		}
		return totalFlow;
	}

	void printAllPaths()
	{
		for (std::vector<Arc> route : foundPaths) 
		{
			for (Arc path : route) 
			{
				std::cout << "<" << path.first << "," << path.second << "," << path.currentCost << "> ";
			}
			std::cout << std::endl;
		}
	}

	void update(std::vector<Arc> & path, double flow) 
	{
		for (Arc & iterator : path) 
		{
			iterator.currentCost += flow * iterator.type - iterator.lowerCapacity;
			for (Arc & arc : nodes[graph.getNodeByName(iterator.second)].arcs) 
			{
				if (iterator.first[0] == arc.second[0]) 
				{
					arc.currentCost = iterator.currentCost;
				}
			}
		}
	}

	void resetFlow() 
	{
		for (Arc & iterator : arcs) 
		{
			iterator.currentCost = 0;
		}
	}
	
};

