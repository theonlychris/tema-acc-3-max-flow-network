#pragma once
#include "Arc.h"
#include <vector>
class Node
{
public:
	Node() = default;
	Node(const std::string & name);
public:
	std::string name;
	double receivedFlow = 0.0;
	double sentFlow = 0.0;
	std::vector<Arc> arcs;
};

